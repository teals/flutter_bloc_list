import 'dart:io';

import 'package:flutter/material.dart';
import 'post_bloc.dart';
import '../repository/api.dart';

class PostProvider extends InheritedWidget {
  final PostBloc postBloc;

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => true;

  static PostBloc of(BuildContext context) =>  // 스태틱으로 해서 1번만 초기화함
  (context.inheritFromWidgetOfExactType(PostProvider) as PostProvider).postBloc; 

  PostProvider({Key key,PostBloc postBloc, Widget child})
  : this.postBloc = postBloc ?? PostBloc(API()), super(child:child,key:key);
}