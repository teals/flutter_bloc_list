import 'dart:async';
import '../model/post.dart';
import '../repository/api.dart';

//블록은 항상 Provider를 통해서 사용
class PostBloc {
  final API api;
  StreamController <List<Post>> ctrl = StreamController();

  Stream<List<Post>> get results => ctrl.stream; // 곧바로 스트림에 접근하지 않기위해 만듬

  PostBloc(this.api);

  void dispose()
  {
    ctrl.close();
  }

  void getPost()
  {
    api.getPosts().then((posts){
      ctrl.add(posts);
    });
  }
}