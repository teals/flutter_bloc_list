
import 'package:flutter_bloc_list/bloc/post_bloc.dart';
import 'package:flutter/material.dart';
import '../bloc/post_provider.dart';

class PostPage extends StatefulWidget {
  @override
  _PostPageState createState() => _PostPageState();
}
  String userId;
  int postId;
  String title;
  String content;
// 스냅샷이 뭐지?
class _PostPageState extends State<PostPage>
{
  Widget _buildListTile (AsyncSnapshot snapshot, int index)
  {
    var userId = snapshot.data[index].userId;
    var postId = snapshot.data[index].postId;
    var title = snapshot.data[index].title;
    var content = snapshot.data[index].content;

    return ListTile(
      leading: Text("$userId"),
      title: Text("$title"),
      subtitle: Text("$content"),
    );
  }
  Widget build(BuildContext context)
  {
    final postBloc = PostProvider.of(context);

    return Scaffold(
      appBar: AppBar(title:Text("Bloc 리스트")),
      body : Center(
        child: Column(
          children: <Widget>[
            RaisedButton(
              color: Colors.blueAccent,
              child:Text("Load Data"),
              onPressed: (){
                postBloc.getPost();
              },
            ),
            Flexible(
              child: StreamBuilder (
                stream: postBloc.results,
                builder: (context, snapshot)
                {
                  if(!snapshot.hasData)
                    return(Text("데이터가 없습니다."));
                  else
                    return ListView.builder(
                      itemCount: snapshot.data.length,
                      itemBuilder: (context, index) => _buildListTile(snapshot, index),
                    );
                },),)
          ],),)
      );
  }
}